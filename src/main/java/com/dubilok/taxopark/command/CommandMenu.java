package com.dubilok.taxopark.command;

import com.dubilok.taxopark.cars.Car;
import com.dubilok.taxopark.company.Company;
import com.dubilok.taxopark.company.UberCompanyImpl;
import com.dubilok.taxopark.company.UklonCompanyImpl;
import com.dubilok.taxopark.service.TaxoPark;
import java.util.List;
import java.util.Scanner;

public class CommandMenu {

    private static Scanner scanner = new Scanner(System.in);

    public void run(TaxoPark taxoPark) {
        while (true) {
            System.out.println("If you want to use Uklon company enter: 'uklon', if Uber enter: 'uber'\n" +
                    "if you want to finish your work enter: '0';");
            String nameCompany = scanner.nextLine();
            if (nameCompany.equals(Command.EXIT.getMessage())) {
                break;
            }
            Company company = getCompany(nameCompany);
            if (company == null) {
                while (true) {
                    nameCompany = scanner.nextLine();
                    company = getCompany(nameCompany);
                    if (company != null) {
                        break;
                    }
                }
            }
            while (true) {
                System.out.println("Enter your data:\n 1 -> to see all cars;\n 2-> to see price autoPark;\n 3-> to sort " +
                        "list by average fuel consuption;\n 4-> get cars with specified parameter;\n 0-> change company;");
                String str = scanner.next();
                if (str.equals(Command.LIST_ALL_CARS.getMessage())) {
                    printAllCars(company);
                } else if (str.equals(Command.PRICE_CARS.getMessage())) {
                    System.out.println(taxoPark.getCarsPrice(company.getCars()));
                } else if (str.equals(Command.SORT_BY_AVERAGE_FUEL_CONSUPTION.getMessage())) {
                    taxoPark.sortByAverageFuelConsumption(company.getCars());
                    printAllCars(company);
                } else if (str.equals(Command.GET_CARS_WITH_SPECIFIED_PARAMETR.getMessage())) {
                    getListCarsWithSpecifiedParameter(scanner, taxoPark, company);
                } else if (str.equals(Command.EXIT.getMessage())) {
                    System.out.println("End!");
                    scanner.nextLine();
                    break;
                }
                System.out.println("------------------------------------------------------------------------------------");
            }

        }
        closeScanner();
    }

    private void getListCarsWithSpecifiedParameter(Scanner scanner, TaxoPark taxoPark, Company company) {
        System.out.println("Enter Your speed parameter: ");
        int speed = scanner.nextInt();
        List<Car> listCar = taxoPark.getListCarsWithSpecifiedParameter(company.getCars(), speed);
        while (true) {
            if (listCar.size() == 0) {
                System.out.println("Car with parametr{" + speed + "} is not found!\nRepeat your choise?" +
                        "\n1->Yes\n0->No");
                scanner.nextLine();
                String repeat = scanner.nextLine();
                System.out.println("Enter your speed: ");
                speed = scanner.nextInt();
                listCar = taxoPark.getListCarsWithSpecifiedParameter(company.getCars(), speed);
                if (repeat.equals("0")) {
                    break;
                }
            } else {
                listCar.forEach(System.out::println);
                break;
            }
        }
    }

    private void closeScanner() {
        scanner.close();
    }

    private Company getCompany(String nameCompany) {
        Company company = null;
        if (nameCompany.equals(Command.UBER.getMessage())) {
            company = UberCompanyImpl.getInstance();
        } else if (nameCompany.equals(Command.UKLON.getMessage())) {
            company = UklonCompanyImpl.getInstance();
        }
        return company;
    }

    private void printAllCars(Company company) {
        company.getCars().forEach(System.out::println);
    }

}
