package com.dubilok.taxopark.command;

public enum Command {
    UBER("uber"),
    UKLON("uklon"),
    LIST_ALL_CARS("1"),
    PRICE_CARS("2"),
    SORT_BY_AVERAGE_FUEL_CONSUPTION("3"),
    GET_CARS_WITH_SPECIFIED_PARAMETR("4"),
    EXIT("0");

    private final String message;

    Command(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
