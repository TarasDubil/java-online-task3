package com.dubilok.taxopark;

import com.dubilok.taxopark.command.CommandMenu;
import com.dubilok.taxopark.service.TaxoParkImpl;

public class Main {

    public static void main(String[] args) {
        CommandMenu commandMenu = new CommandMenu();
        commandMenu.run(new TaxoParkImpl());
    }

}



