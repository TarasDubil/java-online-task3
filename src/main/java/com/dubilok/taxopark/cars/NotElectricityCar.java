package com.dubilok.taxopark.cars;

public abstract class NotElectricityCar extends Car {

    public int fuelTankCapacity;

    public NotElectricityCar(String name, double price, int year, int maxSpeed, int seatsNumber, int mileage,
                             double averageFuelConsumption, Transmition transmition, TypeOfCar typeOfCar, int fuelTankCapacity) {
        super(name, price, year, maxSpeed, seatsNumber, mileage, averageFuelConsumption, transmition, typeOfCar);
        this.fuelTankCapacity = fuelTankCapacity;
    }

    public int getFuelTankCapacity() {
        return fuelTankCapacity;
    }

    @Override
    public String toString() {
        return super.toString() +
                "fuelTankCapacity=" + fuelTankCapacity;
    }
}
