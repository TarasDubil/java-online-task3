package com.dubilok.taxopark.cars;

public class GasCar extends NotElectricityCar {

    public GasCar(String name, double price, int year, int maxSpeed, int seatsNumber, int mileage,
                  double averageFuelConsumption, Transmition transmition, TypeOfCar typeOfCar, int fuelTankCapacity) {
        super(name, price, year, maxSpeed, seatsNumber, mileage, averageFuelConsumption, transmition, typeOfCar,
                fuelTankCapacity);
        setTypeOfFuel();
    }

    @Override
    public TypeOfFuel getTypeOfFuel() {
        return TypeOfFuel.GASOLINE;
    }

    @Override
    public void setTypeOfFuel() {
        this.typeOfFuel = TypeOfFuel.GASOLINE;
    }
}
