package com.dubilok.taxopark.cars;

public enum TypeOfFuel {
    GASOLINE,
    GAS,
    DIESEL,
    ELECTRICITY,
    HYBRID;
}
