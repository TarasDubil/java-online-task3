package com.dubilok.taxopark.cars;

public enum Transmition {
    AUTOMATIC,
    MANUAL_GEARBOX;
}
