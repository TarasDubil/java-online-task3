package com.dubilok.taxopark.cars;

public class HybridCar extends Car {

    private int lifetimeBattery;
    private int chargingTime;
    private int drivingTime;
    public int fuelTankCapacity;

    public HybridCar(String name, double price, int year, int maxSpeed, int seatsNumber, int mileage,
                     double averageFuelConsumption, Transmition transmition, TypeOfCar typeOfCar,
                     int lifetimeBattery, int chargingTime, int drivingTime, int fuelTankCapacity) {
        super(name, price, year, maxSpeed, seatsNumber, mileage, averageFuelConsumption, transmition, typeOfCar);
        this.lifetimeBattery = lifetimeBattery;
        this.chargingTime = chargingTime;
        this.drivingTime = drivingTime;
        this.fuelTankCapacity = fuelTankCapacity;
        setTypeOfFuel();
    }

    public int getLifetimeBattery() {
        return lifetimeBattery;
    }

    public int getChargingTime() {
        return chargingTime;
    }

    public int getDrivingTime() {
        return drivingTime;
    }

    public int getFuelTankCapacity() {
        return fuelTankCapacity;
    }

    @Override
    public TypeOfFuel getTypeOfFuel() {
        return TypeOfFuel.HYBRID;
    }

    @Override
    public void setTypeOfFuel() {
        this.typeOfFuel = TypeOfFuel.HYBRID;
    }

    @Override
    public String toString() {
        return super.toString() +
                "lifetimeBattery=" + lifetimeBattery +
                ", chargingTime=" + chargingTime +
                ", drivingTime=" + drivingTime +
                ", fuelTankCapacity=" + fuelTankCapacity;
    }
}
