package com.dubilok.taxopark.cars;

public abstract class Car {

    private String name;
    private double price;
    private int year;
    private int maxSpeed;
    private int seatsNumber;
    private int mileage;
    private double averageFuelConsumption;
    private Transmition transmition;
    private TypeOfCar typeOfCar;
    protected TypeOfFuel typeOfFuel;

    public Car(String name, double price, int year, int maxSpeed, int seatsNumber, int mileage, double averageFuelConsumption,
               Transmition transmition, TypeOfCar typeOfCar) {
        this.name = name;
        this.price = price;
        this.year = year;
        this.maxSpeed = maxSpeed;
        this.seatsNumber = seatsNumber;
        this.mileage = mileage;
        this.averageFuelConsumption = averageFuelConsumption;
        this.transmition = transmition;
        this.typeOfCar = typeOfCar;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getYear() {
        return year;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public int getMileage() {
        return mileage;
    }

    public double getAverageFuelConsumption() {
        return averageFuelConsumption;
    }

    public Transmition getTransmition() {
        return transmition;
    }

    public TypeOfCar getTypeOfCar() {
        return typeOfCar;
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", price=" + price +
                ", year=" + year +
                ", maxSpeed=" + maxSpeed +
                ", seatsNumber=" + seatsNumber +
                ", mileage=" + mileage +
                ", averageFuelConsumption=" + averageFuelConsumption +
                ", transmition=" + transmition +
                ", typeOfCar=" + typeOfCar +
                ", typeOfFuel=" + typeOfFuel +
                '}';
    }

    public abstract TypeOfFuel getTypeOfFuel();

    public abstract void setTypeOfFuel();
}
