package com.dubilok.taxopark.cars;

public enum TypeOfCar {
    HATCHBACK,
    SEDAN,
    LIMUSINE,
    VAN,
    COUPE,
    SUV,
    SPORT_CAR,
    CONVERTIBLE;
}
