package com.dubilok.taxopark.cars;

public class ElectricityCar extends Car {

    private int lifetimeBattery;
    private int chargingTime;
    private int drivingTime;

    public ElectricityCar(String name, double price, int year, int maxSpeed, int seatsNumber,
                          int mileage, double averageFuelConsumption, Transmition transmition, TypeOfCar typeOfCar,
                          int lifetimeBattery, int chargingTime, int drivingTime) {
        super(name, price, year, maxSpeed, seatsNumber, mileage, averageFuelConsumption, transmition, typeOfCar);
        this.lifetimeBattery = lifetimeBattery;
        this.chargingTime = chargingTime;
        this.drivingTime = drivingTime;
        setTypeOfFuel();
    }

    public int getLifetimeBattery() {
        return lifetimeBattery;
    }

    public int getChargingTime() {
        return chargingTime;
    }

    public int getDrivingTime() {
        return drivingTime;
    }

    @Override
    public TypeOfFuel getTypeOfFuel() {
        return TypeOfFuel.ELECTRICITY;
    }

    @Override
    public void setTypeOfFuel() {
        this.typeOfFuel = TypeOfFuel.ELECTRICITY;
    }

    @Override
    public String toString() {
        return super.toString() +
                "lifetimeBattery=" + lifetimeBattery +
                ", chargingTime=" + chargingTime +
                ", drivingTime=" + drivingTime;
    }
}
