package com.dubilok.taxopark.cars;

public class DieselCar extends NotElectricityCar {

    public DieselCar(String name, double price, int year, int maxSpeed, int seatsNumber, int mileage,
                     double averageFuelConsumption, Transmition transmition, TypeOfCar typeOfCar, int fuelTankCapacity) {
        super(name, price, year, maxSpeed, seatsNumber, mileage, averageFuelConsumption, transmition, typeOfCar,
                fuelTankCapacity);
        setTypeOfFuel();
    }

    @Override
    public TypeOfFuel getTypeOfFuel() {
        return TypeOfFuel.DIESEL;
    }

    @Override
    public void setTypeOfFuel() {
        this.typeOfFuel = TypeOfFuel.DIESEL;
    }
}
