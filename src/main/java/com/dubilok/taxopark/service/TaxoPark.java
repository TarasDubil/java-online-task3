package com.dubilok.taxopark.service;

import com.dubilok.taxopark.cars.Car;

import java.util.List;

public interface TaxoPark {

    double getCarsPrice(List<Car> cars);

    void sortByAverageFuelConsumption(List<Car> cars);

    List<Car> getListCarsWithSpecifiedParameter(List<Car> cars, int speed);

}
