package com.dubilok.taxopark.service;

import com.dubilok.taxopark.cars.Car;

import java.util.*;
import java.util.stream.Collectors;

public class TaxoParkImpl implements TaxoPark {

    public double getCarsPrice(List<Car> cars) {
        int sum = 0;
        for (Car c : cars) {
            sum += c.getPrice();
        }
        return sum;
    }

    public void sortByAverageFuelConsumption(List<Car> cars) {
        cars = cars.stream().sorted(Comparator.comparingDouble(Car::getAverageFuelConsumption)).collect(Collectors.toList());
    }

    public List<Car> getListCarsWithSpecifiedParameter(List<Car> cars, int speed) {
        List<Car> carList = cars.stream().filter(i -> i.getMaxSpeed() >= speed).collect(Collectors.toList());
        return carList;
    }

}
