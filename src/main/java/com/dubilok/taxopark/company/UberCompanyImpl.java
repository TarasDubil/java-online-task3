package com.dubilok.taxopark.company;

import com.dubilok.taxopark.cars.*;

import java.util.ArrayList;
import java.util.List;

public class UberCompanyImpl implements Company {

    public static UberCompanyImpl instance;
    private List<Car> cars = new ArrayList<>(3);

    private UberCompanyImpl() {}

    {
        cars.add(new GasCar("Lada Kaluna", 4000, 2010, 300,
                5, 0, 10, Transmition.MANUAL_GEARBOX, TypeOfCar.SEDAN,
                50));
        cars.add(new GasCar("Lada Kaluna", 4000, 2010, 200,
                5, 0, 11, Transmition.MANUAL_GEARBOX, TypeOfCar.SEDAN,
                50));
        cars.add(new DieselCar("Ford Galaxy", 5000, 2000, 200,
                7, 0, 8, Transmition.MANUAL_GEARBOX, TypeOfCar.HATCHBACK,
                60));
    }

    public static UberCompanyImpl getInstance() {
        if (instance == null) {
            instance = new UberCompanyImpl();
        }
        return instance;
    }

    public List<Car> getCars() {
        return cars;
    }

    @Override
    public String toString() {
        return "UberCompanyImpl{" +
                "cars=" + cars +
                '}';
    }
}
