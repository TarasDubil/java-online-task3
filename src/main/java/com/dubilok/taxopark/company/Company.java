package com.dubilok.taxopark.company;

import com.dubilok.taxopark.cars.Car;

import java.util.List;

public interface Company {
    List<Car> getCars();
}
