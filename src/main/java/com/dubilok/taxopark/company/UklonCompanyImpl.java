package com.dubilok.taxopark.company;

import com.dubilok.taxopark.cars.*;

import java.util.ArrayList;
import java.util.List;

public class UklonCompanyImpl implements Company {


    public static UklonCompanyImpl instance;
    private List<Car> cars = new ArrayList<>(3);

    {
        cars.add(new HybridCar("Toyota Prius", 10000, 2016, 270,
                5, 0, 5, Transmition.AUTOMATIC, TypeOfCar.SEDAN,
                5, 7, 4, 30));
        cars.add(new GasCar("Lada Kaluna", 4000, 2010, 200,
                5, 0, 6, Transmition.MANUAL_GEARBOX, TypeOfCar.SEDAN,
                50));
        cars.add(new DieselCar("Ford Galaxy", 5000, 2000, 200,
                7, 0, 8, Transmition.MANUAL_GEARBOX, TypeOfCar.HATCHBACK,
                60));
    }

    public static UklonCompanyImpl getInstance() {
        if (instance == null) {
            instance = new UklonCompanyImpl();
        }
        return instance;
    }

    public List<Car> getCars() {
        return cars;
    }

    @Override
    public String toString() {
        return "UklonCompanyImpl{" +
                "cars=" + cars +
                '}';
    }
}
