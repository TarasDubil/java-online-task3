package com.dubilok.exception.myexception;

public class FileSizeException extends Exception {

    public FileSizeException(String message) {
        super(message);
    }
}
