package com.dubilok.exception.myexception;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try(FileInputStream fileInputStream = new FileInputStream("pom.xml")) {
            if (fileInputStream.available() > 500) {
                throw new FileSizeException("File size is more than 500!");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FileSizeException e) {
            e.printStackTrace();
        }
    }

}
