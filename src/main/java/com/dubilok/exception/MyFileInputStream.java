package com.dubilok.exception;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class MyFileInputStream extends FileInputStream implements AutoCloseable {

    private FileInputStream inputStream;

    public MyFileInputStream(String name) throws FileNotFoundException {
        super(name);
        this.inputStream = new FileInputStream(name);
    }

    @Override
    public int read() throws IOException {
        return inputStream.read();
    }

    @Override
    public int read(byte[] b) throws IOException {
        return inputStream.read(b);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return inputStream.read(b, off, len);
    }

    @Override
    public int available() throws IOException {
        return inputStream.available();
    }

    @Override
    public void close() throws IOException {
        throw new IOException("Problem with EPAM!");
    }
}
