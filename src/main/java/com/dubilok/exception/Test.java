package com.dubilok.exception;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Test {

    public static void main(String[] args) {
        try (MyFileInputStream myFileInputStream = new MyFileInputStream("pom.xml")) {
            while (myFileInputStream.available() > 0) {
                System.out.print((char) myFileInputStream.read());
            }
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
